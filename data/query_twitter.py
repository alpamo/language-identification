# -*- coding: utf-8 -*-
import twitterscraper, json
from twitterscraper.main import *


# these are languages detected by pycld2;
# let's query only languages present in twitter as stated on the advanced search page
# https://twitter.com/search-advanced?lang=en
langs = [# ('ALBANIAN', 'sq'),
         ('ARABIC', 'ar'), ('ARMENIAN', 'hy'),
         # ('AZERBAIJANI', 'az'), ('BASHKIR', 'ba'), ('BASQUE', 'eu'), ('BELARUSIAN', 'be'),
         # ('BENGALI', 'bn'), ('BOSNIAN', 'bs'), ('BRETON', 'br'),
         ('BULGARIAN', 'bg'), #('BURMESE', 'my'), ('CATALAN', 'ca'),
         ('CROATIAN', 'hr'), ('CZECH', 'cs'), ('Chinese', 'zh'), # ('ChineseT', 'zh-Hant'),
         ('DANISH', 'da'), ('DUTCH', 'nl'),
         ('ENGLISH', 'en'), ('ESTONIAN', 'et'),
         #('FAROESE', 'fo'),
         ('FINNISH', 'fi'), ('FRENCH', 'fr'), #('FRISIAN', 'fy'),
         #('GALICIAN', 'gl'),
         ('GEORGIAN', 'ka'), ('GERMAN', 'de'),
         ('GREEK', 'el'), #('GREENLANDIC', 'kl'), 
         #('HAITIAN_CREOLE', 'ht'), ('HAWAIIAN', 'haw'),
         ('HEBREW', 'iw'),
         ('HINDI', 'hi'), ('HUNGARIAN', 'hu'), ('ICELANDIC', 'is'),
         ('INDONESIAN', 'id'), ('IRISH', 'ga'), ('ITALIAN', 'it'),
         # ('Ignore', 'xxx'),
         #('JAVANESE', 'jw'),
         ('Japanese', 'ja'), #('KANNADA', 'kn'),
         #('KASHMIRI', 'ks'),
         ('KAZAKH', 'kk'), ('KHMER', 'km'),
         ('KURDISH', 'ku'), ('KYRGYZ', 'ky'), ('Korean', 'ko'),
         #('LAOTHIAN', 'lo'),
         ('LATVIAN', 'lv'), ('LITHUANIAN', 'lt'), ('LUXEMBOURGISH', 'lb'),
         ('MACEDONIAN', 'mk'), #('MALAGASY', 'mg'), ('MALAY', 'ms'), ('MALAYALAM', 'ml'),
         ('MALTESE', 'mt'), ('ROMANIAN', 'ro'), ('MONGOLIAN', 'mn'),
         ('MONTENEGRIN', 'sr-ME'), ('NEPALI', 'ne'),
         ('NORWEGIAN', 'no'), #('OCCITAN', 'oc'), ('OSSETIAN', 'os'),
         ('PASHTO', 'ps'), ('PERSIAN', 'fa'), ('POLISH', 'pl'),
         ('PORTUGUESE', 'pt'), ('PUNJABI', 'pa'), ('ROMANIAN', 'ro'), ('RUSSIAN', 'ru'),
         #('SAMOAN', 'sm'), ('SCOTS', 'sco'),
         #('SCOTS_GAELIC', 'gd'),
         ('SERBIAN', 'sr'), ('SLOVAK', 'sk'), ('SLOVENIAN', 'sl'),
         #('SOMALI', 'so'),
         ('SPANISH', 'es'), #('SUNDANESE', 'su'), ('SWAHILI', 'sw'),
         ('SWEDISH', 'sv'), #('SYRIAC', 'syr'), ('TAJIK', 'tg'),
         ('TAMIL', 'ta'), ('TATAR', 'tt'), ('TELUGU', 'te'), ('THAI', 'th'), ('TIBETAN', 'bo'),
         ('TURKISH', 'tr'), #('TURKMEN', 'tk'), ('TWI', 'tw'), ('UIGHUR', 'ug'),
         ('UKRAINIAN', 'uk')]#, ('URDU', 'ur'), ('UZBEK', 'uz'), ('VIETNAMESE', 'vi'),('WELSH', 'cy')]


for lang in langs:
    try:
        query = 'l=' + lang[1] + '&q=since%3A2018-03-30%20until%3A2018-03-30&src=typd'
        tweets = twitterscraper.query.query_tweets_once(query)

        with open('tweets_{}.json'.format(lang[1]), 'w') as outfile:
            dump(tweets, outfile, cls=JSONEncoder)
    except:
        continue
