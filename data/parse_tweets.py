import json, codecs, sys, os

path, out_path = sys.argv[1], sys.argv[2]

for item in os.listdir(path):
    if not os.path.isfile(os.path.join(path, item)):
        continue
    with codecs.open(os.path.join(path, item), "r", "utf-8") as f:
        tweets = json.load(f, encoding='utf-8')

    lang = item.split('_')[1][:-4]
    out = codecs.open(os.path.join(out_path, item) + ".txt", "w", "utf-8")

    for tweet in tweets:
        if tweet['language'] == lang:
            out.write(tweet['text'] + "\t{}\n".format(lang))
