from __future__ import division
from sys import version_info
import json, sys, codecs, os
from LangDetector import *

VERSION = version_info.major

path = sys.argv[1]

for f in os.listdir(path):
    if not f.endswith('.txt'):
        continue
    fpath = os.path.join(path, f)
    inp = codecs.open(fpath, 'r', 'utf-8')
    tp, fp = 0, 0
    lang_detector = LanguageDetector()
    for line in inp:
        try:
            text, lang = line.strip().split('\t')
        except Exception as ex:
            continue
        try:
            det = lang_detector.detect_string(text)
        except Exception as ex:
            continue
        if lang == det:
            tp+=1
        else:
            fp+=1
    if VERSION == 2:
        print "precision for {} ___ LanguageDetector: {}".format(f, tp / (tp + fp))
    else:
        print("precision for {} ___ LanguageDetector: {}".format(f, tp / (tp + fp)))
