import os, sys, email
import numpy as np
import pandas as pd

# download and unpack Enron email dataset
# from https://www.kaggle.com/wcukierski/enron-email-dataset

def get_text_from_email(msg):
    parts = []
    for part in msg.walk():
        if part.get_content_type() == 'text/plain':
            parts.append( part.get_payload() )
    return ''.join(parts)


emails_df = pd.read_csv('emails.csv')
# this requires about 4.5 GB RAM
messages = list(map(email.message_from_string, emails_df['message']))
emails_df.drop('message', axis=1, inplace=True)
keys = messages[0].keys()
for key in keys:
    emails_df[key] = [doc[key] for doc in messages]
    emails_df['content'] = list(map(get_text_from_email, messages))

# let's select random 100 messages
random_msgs = emails_df.sample(100, random_state=1234)
detected_msgs = []
# and detect languages
for i, msg in random_msgs.iterrows():
    detected_msgs.append(lang_detector.detect_document(msg['content']))
detected_msgs = pd.Series(detected_msgs)
random_msgs['det_content'] = detected_msgs.values
random_msgs.to_csv(r'enron_random_100.txt',
                   columns=['content'], header=False, index=False, encoding='utf-8')
random_msgs.to_csv(r'enron_random_100_det.txt',
                   columns=['det_content'], header=False, index=False, encoding='utf-8')
