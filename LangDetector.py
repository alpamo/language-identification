# -*- coding: utf-8 -*-

from __future__ import division
from sys import version_info
import codecs, json
from bisect import bisect_left
from collections import Counter

import pycld2, langdetect
from langid.langid import LanguageIdentifier, model

import nltk


VERSION = version_info.major


# check if NLTK tokenizer data is downloaded
try:
    nltk.data.find('tokenizers/punkt')
except LookupError:
    nltk.download('punkt')


# cumstom tokenizer to preserve initial whitespace
from nltk.tokenize import punkt
class CustomLanguageVars(punkt.PunktLanguageVars):

    _period_context_fmt = r"""
        \S*                          
        %(SentEndChars)s             
        \s*                       #  preserve whitespace
        (?=(?P<after_tok>
            %(NonWord)s
            |
            (?P<next_tok>\S+)
        ))"""


class LanguageDetector():
    
    DEFAULT_DETECTORS = ('pycld2', 'langid', 'langdetect')
    DEFAULT_SENTENCE_SPLITTER = punkt.PunktSentenceTokenizer(lang_vars=CustomLanguageVars())
    DEFAULT_HINT_LANGUAGE = 'en'
    
    
    class Pycld2Detector():
        
        def __init__(self, hint_language):
            self.hint_language = hint_language

        def __make_prob(self, res):
            return res[2][0][2] / 100 if res[2][0][2] > 0 else 0

        def detect_ranges(self, bytestring):
            return pycld2.detect(bytestring,
                                 returnVectors=True,
                                 bestEffort=True,
                                 hintLanguage=self.hint_language)[3]
        
        def detect(self, s):
            res = pycld2.detect(s.encode('utf-8'),
                                bestEffort=True,
                                hintLanguage=self.hint_language)
            return (res[2][0][1], self.__make_prob(res))


    class LangidDetector():
        
        def __init__(self):
                    self.langid_detector = LanguageIdentifier.from_modelstring(model, norm_probs=True)
        
        def detect(self, s):
            return self.langid_detector.classify(s)


    class LangdetectDetector():
        
        def detect(self, s):
            try:
                return (langdetect.detect_langs(s)[0].lang,
                        langdetect.detect_langs(s)[0].prob)
            except:
                return ('un', 0)
            

    def make_detectors(self, detector_names):
        detectors = {}
        for detector_name in detector_names:
            if detector_name == 'pycld2':
                detector = LanguageDetector.Pycld2Detector(hint_language=self.hint_language)
            elif detector_name == 'langid':
                detector = LanguageDetector.LangidDetector()
            else:
                detector = LanguageDetector.LangdetectDetector()
            detectors[detector_name] = detector
        return detectors


    def __init__(self, detectors=DEFAULT_DETECTORS, sentence_splitter=DEFAULT_SENTENCE_SPLITTER, hint_language=DEFAULT_HINT_LANGUAGE):
        assert 'pycld2' in detectors, 'pycld2 must be used as one of the detectors'
        self.hint_language = hint_language  
        self.detectors = self.make_detectors(detectors)
        self.sentence_splitter = sentence_splitter
        

    def __choose_best(self, det_langs):
        return max(det_langs, key=lambda x: x[1])

    
    def detect_ranges(self, s):
        return self.detectors['pycld2'].detect_ranges(s)

    
    def detect_string(self, s):
        det_langs = [self.detectors[detector].detect(s) for detector in self.detectors]
        # lets's check the outputs
        unique_langs = set(det_langs)
        # if all detectors output the same language
        if len(unique_langs) == 1:
            return det_langs[0][0]
        # if all detectors output different languages - let's take the one with highest prob
        if len(unique_langs) == len(det_langs):
            return self.__choose_best(det_langs)[0]
        # otherwise take the most frequent detected language
        return Counter(det_langs).most_common(1)[0][0][0]


    def __text_to_ranges(self, text):
        """
        Returns sentence ranges for text split by sentences.
        """
        text = self.sentence_splitter.tokenize(text)
        text_ranges, length = [], 0
        for sent in text:
            sent = sent.encode('utf-8')
            text_ranges.append(length + len(sent))
            length += len(sent)
        return text_ranges


    def __find_closest(self, my_list, number):
            """
            Returns closest value in a list given a number.
            If two numbers are equally close, returns the smallest number.
            """
            pos = bisect_left(my_list, number)
            if pos == 0:
                return my_list[0]
            if pos == len(my_list):
                return my_list[-1]
            before = my_list[pos - 1]
            after = my_list[pos]
            return after if (after - number < number - before) else before


    def __map_pycld2_vectors(self, pycld2_vectors, text_ranges):
        """
        Returns vectors mapped to sentence boundaries.
        """
        mapped_vectors = []
        current_length = 0
        for v in pycld2_vectors:
            closest_sent = self.__find_closest(text_ranges, v[1] + v[0] - 1)
            mapped_vectors.append((current_length, closest_sent - current_length, v[2], v[3]))
            current_length = closest_sent
        return mapped_vectors

        
    def __map_langs(self, bytestring, vectors, return_type='xml'):
        """
        Returns initial text split into language blocks.
        Output is either simple XML of format '<lang1>text</lang1><lang2>text</lang2>..'
        or JSON of format [[text, lang1], [text, lang2], ...]
        """
        det_string = []
        for i, v in enumerate(vectors):
            start, length, lang = v[0], v[1], v[3]
            if length == 0:
                continue
            lang = self.detect_string(bytestring[start:start + length].decode('utf-8')) # this is bad
            try:
                if det_string[-1][1] == lang:
                    det_string[-1][0] += bytestring[start:start + length]
                else:
                    det_string.append([bytestring[start:start + length], lang])
            except IndexError:
                det_string.append([bytestring[start:start + length], lang])
        if return_type == 'xml':
            return ''.join(['<{}>'.format(x[1]) + \
                            x[0].decode('utf-8', errors='ignore') + \
                            '</{}>'.format(x[1]) for x in det_string])
        if return_type == 'json':
            if VERSION == 3:
                return json.dumps([[x[0].decode('utf-8'), x[1]] for x in det_string])
            return json.dumps(det_string)


    def detect_document(self, text, return_type='xml'):
        """
        Takes document text as input and returns the text split into language blocks.
        """
        bytestring = text.encode('utf-8')
        pycld2_vectors = self.detect_ranges(bytestring)
        text_ranges = self.__text_to_ranges(text)
        mapped_pycld2_vectors = self.__map_pycld2_vectors(pycld2_vectors, text_ranges)
        return self.__map_langs(bytestring, mapped_pycld2_vectors, return_type)
    
