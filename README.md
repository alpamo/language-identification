## Usage

```
pip install -r requirements.txt
example_detect.py -i test.txt
```

The result will be written to a file 'test.txt.detected.txt'

Type `example_detect.py -h` for other usage options

The test file is a synthetic test set randomly selected from tweets in different languages.

---

## Description

The module uses three language detection libraries: pycld2, langid and langdetect.  
Splitting of documents into language blocks is based on pycld2, which returns vectors of byte ranges for languages detected in text. The ranges returned by pycld2 are mapped to modified NLTK tokenizer sentence ranges (as pycld2 is not always accurate according to text boundaries). Every piece is scored again by each of the used language detectors. The most frequent output language wins. In case if all detectors output different languages, we take the one with the highest probability score.
The reasons why we don't use any machine learning classification algorithms here are:  
1. The output of each of the detectors is essentially a blackbox.  
2. Each of the detectors is reliable.  
3. We don't want to reinvent the wheel.  

We can train classifiers for new languages for langid and langdetect easily enough, but it seems like we won't need that.

The algorithm can be improved by adding keyword dictionaries (which are already used in pycld2), but this needs to be explored.

---

## Data
We provide two datasets:  
1. Random messages from ENRON emails dataset;  
2. Tweets in different languages collected from Twitter (March, 30)  

---

## Results
The results on ENRON emails look good: all messages were detected as English (which they should be).  
Results on tweets need to be investigated as  
1) the Twitter 'language' field turned out to be unreliable;  
2) several files are too small and therefore not representative;  
3) some tweets are essentially garbage.  

We can notice, however, improvements on large (reliable) and problematic files like, e.g., Spanish and Portuguese.  


Precision on text file | LangDetector | pycld2 | langid | langdetect |
---------------------- | ------------ | ------ | ------ | ---------- |
tweets_ar.json.txt | 0,884 | 0,944 | 0,800 | 0,878 |
tweets_bg.json.txt | 0,667 | 1,000 | 0,667 | 1,000 |
tweets_cs.json.txt | 0,500 | 0,250 | 0,250 | 0,250 |
tweets_da.json.txt | 1,000 | 1,000 | 1,000 | 1,000 |
tweets_de.json.txt | 1,000 | 1,000 | 0,833 | 0,833 |
tweets_en.json.txt | 0,876 | 0,927 | 0,876 | 0,798 |
tweets_es.json.txt | 0,911 | 0,864 | 0,898 | 0,872 |
tweets_et.json.txt | 0,000 | 0,000 | 0,000 | 0,000 |
tweets_fi.json.txt | 1,000 | 1,000 | 1,000 | 1,000 |
tweets_fr.json.txt | 0,880 | 0,726 | 0,894 | 0,861 |
tweets_hi.json.txt | 0,083 | 0,083 | 0,083 | 0,083 |
tweets_hu.json.txt | 0,750 | 0,875 | 0,750 | 0,875 |
tweets_it.json.txt | 0,855 | 0,687 | 0,866 | 0,841 |
tweets_ja.json.txt | 0,903 | 0,861 | 0,915 | 0,824 |
tweets_km.json.txt | 1,000 | 0,000 | 1,000 | 0,000 |
tweets_ko.json.txt | 0,900 | 0,892 | 0,973 | 0,946 |
tweets_lt.json.txt | 0,746 | 0,829 | 0,643 | 0,657 |
tweets_lv.json.txt | 0,852 | 0,869 | 0,852 | 0,836 |
tweets_nl.json.txt | 0,789 | 0,776 | 0,735 | 0,735 |
tweets_pl.json.txt | 0,830 | 0,851 | 0,830 | 0,809 |
tweets_pt.json.txt | 0,863 | 0,815 | 0,784 | 0,821 |
tweets_ro.json.txt | 0,647 | 0,500 | 0,438 | 0,750 |
tweets_ru.json.txt | 0,694 | 0,671 | 0,658 | 0,669 |
tweets_sv.json.txt | 1,000 | 1,000 | 1,000 | 1,000 |
tweets_th.json.txt | 0,937 | 0,906 | 0,935 | 0,889 |
tweets_tr.json.txt | 0,740 | 0,697 | 0,688 | 0,675 |
tweets_uk.json.txt | 0,462 | 0,462 | 0,462 | 0,538 |
tweets_zh.json.txt | 0,940 | 0,798 | 0,991 | 0,000 |

---

## References

**pycld2**
https://github.com/aboSamoor/pycld2

**Langid**
https://github.com/saffsd/langid.py

**Langdetect**
https://pypi.org/project/langdetect/
