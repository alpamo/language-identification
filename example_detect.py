import codecs, sys, argparse
from LangDetector import *


arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-i', '--infile', help='input file', required=True)
arg_parser.add_argument('-f', '--format', help='output format: xml or json',
                        choices=['xml', 'json'],default='xml')
arg_parser.add_argument('-c', '--coding', help='input file coding',
                        default='utf-8')
arg_parser.add_argument('-l', '--hint-language', help='hint language',
                        default='en')
arg_parser.add_argument('-d', '--detectors', help='list of detectors',
                        nargs='+', default=['pycld2', 'langid', 'langdetect'])
args = arg_parser.parse_args()


def main():
    lang_detector = LanguageDetector(hint_language=args.hint_language,
                                     detectors=args.detectors)
    text = codecs.open(args.infile, 'r', args.coding).read()
    detected = lang_detector.detect_document(text, return_type=args.format)
    with codecs.open(args.infile + '.detected.txt', 'w', args.coding) as out:
        out.write(detected)
            
if __name__ == '__main__':
    main()
